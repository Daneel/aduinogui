function(setup_arduino_target_build_info)
    set(BOARD_NAME "" CACHE STRING "Target arduino board name")
    set(BOARD_CPU "" CACHE STRING "Target arduino CPU")

    if (NOT BOARD_NAME)
        message(FATAL_ERROR "BOARD_NAME is not define! What is the target?")
    endif()

    if (NOT BOARD_NAME)
        message(FATAL_ERROR "BOARD_CPU is not define! What is the target?")
    endif()
endfunction()

function(setup_arduino_target_upload_info)
    set(UPLOAD_PORT "" CACHE STRING "Port to upload builded programs")
endfunction()

function(setup_arduino_target_info)
    setup_arduino_target_build_info()
    setup_arduino_target_upload_info()
endfunction()
