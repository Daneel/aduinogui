include(TargetFindAndLinkArduinoLibrary)

set(ETL_DIR ${CMAKE_CURRENT_LIST_DIR}/../external/etl)

function(add_etl_subdirectory)
    if (NOT ARDUINOGUI_BUILD_TESTING)
        set(ETL_PROFILE "PROFILE_ARDUINO" CACHE STRING "Defines what profile header to include.  See https://www.etlcpp.com/setup.html")
    endif()
    add_subdirectory(${ETL_DIR})
endfunction()
