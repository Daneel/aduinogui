function(target_find_and_link_arduino_library _TARGET _SCOPE _LIBRARY_NAME)

    set(options 3RD_PARTY)
    set(oneValueArgs LIBRARY_TARGET_NAME)
    cmake_parse_arguments(target_find_and_link_arduino_library "${options}" "${oneValueArgs}" "" ${ARGN})

    set(3RD_PARTY ${target_find_and_link_arduino_library_3RD_PARTY})
    set(LIBRARY_TARGET_NAME ${target_find_and_link_arduino_library_LIBRARY_TARGET_NAME})

    if (NOT LIBRARY_TARGET_NAME)
        set(LIBRARY_TARGET_NAME ${_LIBRARY_NAME})
    endif()

    if (NOT TARGET ${LIBRARY_TARGET_NAME})
        if (3RD_PARTY)
            find_arduino_library(${LIBRARY_TARGET_NAME} ${_LIBRARY_NAME} 3RD_PARTY)
        else()
            find_arduino_library(${LIBRARY_TARGET_NAME} ${_LIBRARY_NAME})
        endif()

        link_arduino_library(${_TARGET} ${LIBRARY_TARGET_NAME})
    else()
        target_link_libraries(${_TARGET} ${_SCOPE} ${LIBRARY_TARGET_NAME})
    endif()

endfunction()