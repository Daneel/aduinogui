set(EXTERNAL_DIR ${CMAKE_CURRENT_LIST_DIR}/../external)

function(use_arduino_toolchain)
    set(ARDUINO_CMAKE_SKETCHBOOK_PATH "${EXTERNAL_DIR}" CACHE PATH "")
    set(CMAKE_TOOLCHAIN_FILE ${EXTERNAL_DIR}/Arduino-CMake-NG/cmake/Arduino-Toolchain.cmake PARENT_SCOPE)
endfunction()