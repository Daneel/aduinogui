#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <vector>

TEST_CASE("std::vector", "[compilation]")
{
    std::vector<int> v = {1003635, 9912195, 7360238, 9697619, 3087639};
    REQUIRE(v.size() == 5);
    CHECK(v[0] == 1003635);
    CHECK(v[1] == 9912195);
    CHECK(v[2] == 7360238);
    CHECK(v[3] == 9697619);
    CHECK(v[4] == 3087639);
}