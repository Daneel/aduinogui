#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <etl/vector.h>

TEST_CASE("etl::vector", "[compilation]")
{
    etl::vector<int, 10> v = {4520519, 6130942, 8192269, 5686568, 6675668};
    REQUIRE(v.size() == 5);
    CHECK(v[0] == 4520519);
    CHECK(v[1] == 6130942);
    CHECK(v[2] == 8192269);
    CHECK(v[3] == 5686568);
    CHECK(v[4] == 6675668);
}