#include <catch2/catch.hpp>

#include <arduinogui/moc_drawer.hpp>

int drawer_tests_token = 0;

TEST_CASE("MocDrawer simple offset", "positive")
{
	ArduinoGUI::MocDrawer drawer;

	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(0, 0, 800, 600));

	drawer.pushClipRect({150, 250, 800, 600});
	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(150, 250, 650, 350));
	drawer.popClipRect();

	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(0, 0, 800, 600));
}

TEST_CASE("MocDrawer additive offset", "positive")
{
	ArduinoGUI::MocDrawer drawer;

	drawer.pushClipRect({300, 400, 800, 600});

	drawer.pushClipRect({150, 150, 800, 600});
	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(450, 550, 350, 50));
	drawer.popClipRect();

	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(300, 400, 500, 200));

	drawer.popClipRect();

	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(0, 0, 800, 600));
}

TEST_CASE("MocDrawer clip offset min()", "positive")
{
	ArduinoGUI::MocDrawer drawer;

	drawer.pushClipRect({300, 300, 800, 600});
	drawer.pushClipRect({200, 250, 800, 600});
	auto rect = drawer.getClipRect();
	CHECK(rect.x == 500);
	CHECK(rect.y == 550);
	drawer.popClipRect();
	drawer.popClipRect();
}

TEST_CASE("MocDrawer clip rect restore", "positive")
{
	ArduinoGUI::MocDrawer drawer;

	auto startRect = drawer.getClipRect();

	drawer.pushClipRect({0, 0, 100, 200});
	auto rect = drawer.getClipRect();
	drawer.popClipRect();

	CHECK(startRect == drawer.getClipRect());
}

TEST_CASE("MocDrawer clip rect min()", "positive")
{
ArduinoGUI::MocDrawer drawer;

	drawer.pushClipRect({0, 0, 200, 200});
	drawer.pushClipRect({0, 0, 300, 300});
	auto rect = drawer.getClipRect();
	CHECK(rect.width == 200);
	CHECK(rect.height == 200);
	drawer.popClipRect();
	drawer.popClipRect();
}

TEST_CASE("MocDrawer clip rect push/pop chain", "positive")
{
	ArduinoGUI::MocDrawer drawer;

	drawer.pushClipRect({100, 100, 800, 600});

	drawer.pushClipRect({100, 100, 700, 500});
	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(200, 200, 600, 400));
	drawer.popClipRect();

	drawer.pushClipRect({50, 30, 300, 300});
	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(150, 130, 300, 300));
	drawer.popClipRect();

	drawer.pushClipRect({400, 400, 80, 80});
		CHECK(drawer.getClipRect() == ArduinoGUI::Rect(500, 500, 80, 80));
			drawer.pushClipRect({50, 30, 20, 10});
			CHECK(drawer.getClipRect() == ArduinoGUI::Rect(550, 530, 20, 10));
			drawer.popClipRect();
		CHECK(drawer.getClipRect() == ArduinoGUI::Rect(500, 500, 80, 80));
	drawer.popClipRect();
	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(100, 100, 700, 500));

	drawer.popClipRect();

	CHECK(drawer.getClipRect() == ArduinoGUI::Rect(0, 0, 800, 600));
}
