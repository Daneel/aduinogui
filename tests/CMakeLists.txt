cmake_minimum_required(VERSION 3.8.2)

project(tests C CXX ASM)

add_subdirectory(42_tests)
add_subdirectory(stl_tests)
add_subdirectory(etl_tests)
add_subdirectory(arduinogui_lib_tests)
