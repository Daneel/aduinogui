#include "mcufriend_kbv_clipped.hpp"

#include "clamp.hpp"

#include <etl/stl/algorithm.h>

namespace ArduinoGUI
{
void MCUFRIEND_kbv_clipped::drawPixel(int16_t x, int16_t y, uint16_t color)
{
	if (m_clipMinX <= x && x <= m_clipMaxX
		&& m_clipMinY <= y && y <= m_clipMaxY)
	{
		Base::drawPixel(x, y, color);
	}
}
void MCUFRIEND_kbv_clipped::fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
	if (x < m_clipMinX || x > m_clipMaxX || y < m_clipMinY || y > m_clipMaxY)
	{
		return;
	}

	int16_t maxX = x + w;
	int16_t maxY = y + h;

	x = clamp(x, m_clipMinX, m_clipMaxX);
	y = clamp(y, m_clipMinY, m_clipMaxY);
	maxX = clamp(maxX, m_clipMinX, m_clipMaxX);
	maxY = clamp(maxY, m_clipMinY, m_clipMaxY);

	w = maxX - x;
	h = maxY - y;

	Base::fillRect(x, y, w, h, color);
}
void MCUFRIEND_kbv_clipped::setRotation(uint8_t r)
{
	Base::setRotation(r);
}
}
