#include "arduino_application.hpp"

#include <Arduino.h>

namespace ArduinoGUI
{
void ArduinoApplication::setup()
{
	Base::setup();
	m_time = millis();
}

void ArduinoApplication::tick()
{
	uint32_t newTime = millis();
	if (newTime < m_time)
	{
		//overflow(once at 50 days)
		m_time = newTime;
		return;  // lets skip one tick, easyly way to resolve situation
	}

	uint32_t dt = newTime - m_time;

	Base::tick(dt);

	m_time = newTime;
}
}
