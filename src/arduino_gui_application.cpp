
#include <arduino_gui_application.hpp>

namespace ArduinoGUI
{
ArduinoGUIApplication::ArduinoGUIApplication()
	: m_itemSystem(m_drawSystem)
{
	addSystem(m_drawSystem);
	addSystem(m_itemSystem);
}
}
