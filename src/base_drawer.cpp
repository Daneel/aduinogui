#include "base_drawer.hpp"

#include "clamp.hpp"

#include <limits.h>
#include <limits>

namespace ArduinoGUI
{
Rect BaseDrawer::getClipRect()
{
	if (m_clipRectStack.empty())
	{
		return Rect(
			0,
			0,
			std::numeric_limits<int16_t>::max(),
			std::numeric_limits<int16_t>::max());
	}
	else
	{
		return m_clipRectStack.top();
	}
}
void BaseDrawer::pushClipRect(const Rect& rect)
{
	const auto currentRect = getClipRect();

	Rect newRect;

	newRect.x = currentRect.x + rect.x;
	newRect.y = currentRect.y + rect.y;

	newRect.width = clamp<int16_t>(
		rect.width,
		0,
		currentRect.width - rect.x);

	newRect.height = clamp<int16_t>(
		rect.height,
		0,
		currentRect.height - rect.y);

	m_clipRectStack.push(newRect);

	setClippRect(newRect);
}
void BaseDrawer::popClipRect()
{
	m_clipRectStack.pop();

	setClippRect(getClipRect());
}
}

