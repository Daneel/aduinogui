#include "items/elips.hpp"

namespace ArduinoGUI
{
void Elips::drawSelf(BaseDrawer& drawer)
{
	drawer.drawElips(getWidth(), getHeight(), getColor());
}
}
