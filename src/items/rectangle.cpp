#include "items/rectangle.hpp"

namespace ArduinoGUI
{
void Rectangle::drawSelf(BaseDrawer& drawer)
{
	drawer.drawRectangle(getWidth(), getHeight(), getColor());
}
}

