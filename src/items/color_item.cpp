#include "items/color_item.hpp"

namespace ArduinoGUI
{
void ColorItem::setColor(Color value)
{
	m_color = value;
	appendChangeState(ChangeState::CONTENT_CHANGED);
}
Color ColorItem::getColor() const
{
	return m_color;
}
}

