#include "opensmart_tft_drawer.hpp"


namespace ArduinoGUI
{

void OpensmartTFTDrawer::setup()
{
	m_tft.begin(0x65);//to enable HX8352B driver code
	m_tft.setRotation(1);
	m_tft.fillScreen(BLACK);
	pushClipRect({0, 0, m_tft.width(), m_tft.height()});
}
void OpensmartTFTDrawer::beginDraw()
{
}
void OpensmartTFTDrawer::endDraw()
{
}
void OpensmartTFTDrawer::drawElips(uint32_t width, uint32_t height, Color color)
{
	const auto clipRect = getClipRect();
	const float a = width / 2.0f;
	const float b = height / 2.0f;
	for (uint32_t y=0; y<height; y++)
	{
		const float q = a * sqrt(1.0f - square(y - b) / square(b));
		const auto xLeft = static_cast<uint32_t>(a - q + 0.5f);
		const auto xRight = static_cast<uint32_t>(a + q - 0.5f);
		m_tft.drawFastHLine(clipRect.x + xLeft, clipRect.y +  y, xRight - xLeft, color);
	}
}
void OpensmartTFTDrawer::drawRectangle(uint32_t width, uint32_t height, Color color)
{
	const auto clipRect = getClipRect();
	m_tft.fillRect(clipRect.x, clipRect.y, width, height, color);
}
void OpensmartTFTDrawer::drawText(const char* text, uint8_t size, Color color)
{
	const auto clipRect = getClipRect();
	m_tft.setTextColor(color);
	m_tft.setTextSize(size);
	m_tft.setCursor(clipRect.x, clipRect.y);
	m_tft.print(text);
}
void OpensmartTFTDrawer::setClippRect(Rect rect)
{
	m_tft.setClipMinX(rect.x);
	m_tft.setClipMinY(rect.y);
	m_tft.setClipMaxX(rect.x + rect.width);
	m_tft.setClipMaxY(rect.y + rect.height);
}
}
