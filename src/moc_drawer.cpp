#include <moc_drawer.hpp>

namespace ArduinoGUI
{
MocDrawer::MocDrawer()
{
	pushClipRect({0, 0, 800, 600});
}
void MocDrawer::beginDraw()
{
}
void MocDrawer::endDraw()
{
}
void MocDrawer::drawElips(uint32_t width, uint32_t height, Color color)
{
}
void MocDrawer::drawRectangle(uint32_t width, uint32_t height, Color color)
{
}
void MocDrawer::drawText(const char* text, uint8_t size, Color color)
{
}
void MocDrawer::setClippRect(Rect rect)
{
}

}
