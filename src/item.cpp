#include "item.hpp"

namespace ArduinoGUI
{
void Item::tick(uint32_t dt)
{
	bool childTransformed = false;
	for (auto& child: m_childs)
	{
		child.tick(dt);
		childTransformed |= child.getChangeState() == ChangeState::TRANSFORMED;
	}

	if (childTransformed)
	{
		if (isTransparent())
		{
			appendChangeState(ChangeState::TRANSFORMED);
		}
		else
		{
			appendChangeState(ChangeState::CONTENT_CHANGED);
		}
	}

	tickSelf(dt);
}
void Item::draw(BaseDrawer& drawer)
{
	drawer.pushClipRect(m_contextRect);

	if (m_changeState != ChangeState::NONE)
	{
		drawSelf(drawer);
	}

	for (auto& child: m_childs)
	{
		child.draw(drawer);
	}

	drawer.popClipRect();
}
void Item::addChild(Item& item)
{
	if (m_last == m_childs.end())
	{
		m_childs.push_front(item);
		m_last = m_childs.begin();
	}
	else
	{
		m_last = m_childs.insert_after(m_last, item);
	}

	item.setParent(this);
}
void Item::setX(int16_t x)
{
	m_contextRect.x = x;
	appendChangeState(ChangeState::TRANSFORMED);
}
int16_t Item::getX() const
{
	return m_contextRect.x;
}
void Item::setY(int16_t y)
{
	m_contextRect.y = y;
	appendChangeState(ChangeState::TRANSFORMED);
}
int16_t Item::getY() const
{
	return m_contextRect.y;
}
void Item::setWidth(int16_t value)
{
	m_contextRect.width = value;
	appendChangeState(ChangeState::TRANSFORMED);
}
int16_t Item::getWidth() const
{
	return m_contextRect.width;
}
void Item::setHeight(int16_t value)
{
	m_contextRect.height = value;
	appendChangeState(ChangeState::TRANSFORMED);
}
int16_t Item::getHeight() const
{
	return m_contextRect.height;
}
void Item::appendChangeState(Item::ChangeState state)
{
	if (state <= m_changeState)
	{
		return;
	}

	m_changeState = state;
	if (m_changeState != Item::ChangeState::NONE)
	{
		for (auto& child: m_childs)
		{
			child.appendChangeState(Item::ChangeState::CONTENT_CHANGED);
		}
	}
}
}
