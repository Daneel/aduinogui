#include "application.hpp"

namespace ArduinoGUI
{

void Application::addSystem(ArduinoGUI::System& system)
{
	if (m_last == m_systems.end())
	{
		m_systems.push_front(system);
		m_last = m_systems.begin();
	}
	else
	{
		m_last = m_systems.insert_after(m_last, system);
	}
}

void Application::setup()
{
	for (auto& system : m_systems)
	{
		system.setup();
	}
}

void Application::tick(uint32_t dt)
{
	for (auto& system : m_systems)
	{
		system.tick(dt);
	}
}
}
