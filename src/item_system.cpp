#include "item_system.hpp"
#include "base_drawer.hpp"

namespace ArduinoGUI
{
ItemSystem::ItemSystem(BaseDrawer& drawer)
	: m_drawer(drawer)
{
	m_rootItem.setColor(BLACK);
}
void ItemSystem::tick(uint32_t dt)
{
	m_rootItem.tick(dt);

	m_drawer.beginDraw();
	m_rootItem.draw(m_drawer);
	m_drawer.endDraw();
}
void ItemSystem::setup()
{
	m_rootItem.setWidth(m_drawer.getClipRect().width);
	m_rootItem.setHeight(m_drawer.getClipRect().height);
}
}
