#pragma once

#include <stdint.h>

namespace ArduinoGUI
{
struct Point
{
	Point() = default;

	Point(int16_t x, int16_t y)
		: x(x)
		, y(y)
	{}

	bool operator==(const Point& other) const
	{
		return x == other.x && y == other.y;
	}

	bool operator!=(const Point& other) const
	{
		return x != other.x || y != other.y;
	}

public:
	int16_t x = 0;
	int16_t y = 0;
};
}
