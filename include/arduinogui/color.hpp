#pragma once

#include <inttypes.h>

namespace ArduinoGUI
{
using Color = uint32_t;

constexpr Color BLACK = 0x0000;
constexpr Color BLUE = 0x001F;
constexpr Color RED = 0xF800;
constexpr Color GREEN = 0x07E0;
constexpr Color CYAN = 0x07FF;
constexpr Color MAGENTA = 0xF81F;
constexpr Color YELLOW = 0xFFE0;
constexpr Color WHITE = 0xFFFF;
}
