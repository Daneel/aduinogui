#pragma once

#include <stdint.h>

namespace ArduinoGUI
{
struct Rect
{
	Rect() = default;

	Rect(int16_t x, int16_t y, int16_t width, int16_t height)
		: x(x)
		, y(y)
		, width(width)
		, height(height)
	{}

	bool operator==(const Rect& other) const
	{
		return width == other.width
			&& height == other.height
			&& x == other.x
			&& y == other.y;
	}

	bool operator!=(const Rect& other) const
	{
		return width != other.width
			|| height != other.height
			|| x != other.x
			|| y != other.y;
	}

public:
	int16_t x = 0;
	int16_t y = 0;
	int16_t width = 0;
	int16_t height = 0;
};
}

