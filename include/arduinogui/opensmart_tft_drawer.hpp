#pragma once

#include "base_drawer.hpp"
#include "system.hpp"

#include "mcufriend_kbv_clipped.hpp"

namespace ArduinoGUI
{
class OpensmartTFTDrawer
	: public BaseDrawer
	, public System
{
public:
	void setup() override;

	void beginDraw() override;
	void endDraw() override;

	void drawElips(uint32_t width, uint32_t height, Color color) override;
	void drawRectangle(uint32_t width, uint32_t height, Color color) override;
	void drawText(const char* text, uint8_t size, Color color) override;

protected:
	void setClippRect(Rect rect) override;

private:
	MCUFRIEND_kbv_clipped m_tft;
};
}
