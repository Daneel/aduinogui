#pragma once

#include "point.hpp"
#include "rect.hpp"
#include "color.hpp"

#include <etl/stack.h>
#include <inttypes.h>

namespace ArduinoGUI
{
class BaseDrawer
{
public:
	Rect getClipRect();
	void pushClipRect(const Rect& rect);
	void popClipRect();

	virtual void beginDraw() = 0;
	virtual void endDraw() = 0;

	virtual void drawElips(uint32_t width, uint32_t height, Color color) = 0;
	virtual void drawRectangle(uint32_t width, uint32_t height, Color color) = 0;
	virtual void drawText(const char* text, uint8_t size, Color color) = 0;

protected:
	virtual void setClippRect(Rect rect) = 0;

private:
	etl::stack<Rect, 32> m_clipRectStack;
};
}
