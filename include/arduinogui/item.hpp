#pragma once

#include "base_drawer.hpp"
#include <etl/intrusive_forward_list.h>

namespace ArduinoGUI
{
class Item
	: public etl::forward_link<0>
{
public:
	enum class ChangeState : uint8_t
	{
		NONE,
		CONTENT_CHANGED,
		TRANSFORMED,
	};

public:

	virtual void tickSelf(uint32_t dt) {}
	virtual void drawSelf(BaseDrawer& drawer) = 0;

	virtual void appendChangeState(ChangeState state);
	virtual ChangeState getChangeState() { return m_changeState; }

	virtual bool isTransparent() const = 0;

	virtual void tick(uint32_t dt);
	virtual void draw(BaseDrawer& drawer);
	virtual void addChild(Item& item);

	virtual void setX(int16_t x);
	virtual int16_t getX() const;
	virtual void setY(int16_t y);
	virtual int16_t getY() const;

	virtual void setWidth(int16_t value);
	virtual int16_t getWidth() const;
	virtual void setHeight(int16_t value);
	virtual int16_t getHeight() const;

	virtual Item* getParent() { return m_parent;}
	virtual void setParent(Item* value) { m_parent = value; }

private:
	etl::intrusive_forward_list<Item> m_childs;
	etl::intrusive_forward_list<Item>::iterator m_last = m_childs.begin();
	Item* m_parent = nullptr;
	ChangeState m_changeState = ChangeState::TRANSFORMED;
	Rect m_contextRect;
};
}
