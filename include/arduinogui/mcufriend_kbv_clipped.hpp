#pragma once

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>

namespace ArduinoGUI
{
class MCUFRIEND_kbv_clipped
	: public MCUFRIEND_kbv
{
	using Base = MCUFRIEND_kbv;
public:
	void setRotation(uint8_t r) override;

	void setClipMinX(int16_t value) { m_clipMinX = value; }
	void setClipMinY(int16_t value) { m_clipMinY = value; }
	void setClipMaxX(int16_t value) { m_clipMaxX = value; }
	void setClipMaxY(int16_t value) { m_clipMaxY = value; }
	int16_t getClipMinX() const { return m_clipMinX; }
	int16_t getClipMinY() const { return m_clipMinY; }
	int16_t getClipMaxX() const { return m_clipMaxX; }
	int16_t getClipMaxY() const { return m_clipMaxY; }

	void drawPixel(int16_t x, int16_t y, uint16_t color) override;
	void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) override;

private:
	int16_t m_clipMinX = INT16_MIN;
	int16_t m_clipMinY = INT16_MIN;
	int16_t m_clipMaxX = INT16_MAX;
	int16_t m_clipMaxY = INT16_MAX;
};
}

