#pragma once

#include "base_drawer.hpp"

namespace ArduinoGUI
{
class MocDrawer
	: public BaseDrawer
{
public:
	MocDrawer();
	void beginDraw() override;
	void endDraw() override;
	void drawElips(uint32_t width, uint32_t height, Color color) override;
	void drawRectangle(uint32_t width, uint32_t height, Color color) override;
	void drawText(const char* text, uint8_t size, Color color) override;
protected:
	void setClippRect(Rect rect) override;
};
}

