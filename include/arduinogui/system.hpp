#pragma once

#include <stdint.h>
#include <etl/intrusive_forward_list.h>

namespace ArduinoGUI
{
class System
	: public etl::forward_link<0>
{
public:
	virtual void setup(){}
	virtual void tick(uint32_t dTime){}
};
}
