#pragma once

#include "system.hpp"
#include "items/rectangle.hpp"


namespace ArduinoGUI
{
class BaseDrawer;

class ItemSystem
	: public System
{
public:
	ItemSystem(BaseDrawer& drawer);
	void tick(uint32_t dt) override;
	void setup() override;

	Rectangle& getRootItem() { return m_rootItem; };

private:
	BaseDrawer& m_drawer;
	Rectangle m_rootItem;
};
}
