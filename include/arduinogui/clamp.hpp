#pragma once

namespace ArduinoGUI
{
template <typename T>
const T& clamp(const T& value, const T& min, const T& max)
{
	if (value < min)
	{
		return min;
	}
	else if(value > max)
	{
		return max;
	}
	else
	{
		return value;
	}
}
}
