#pragma once

#include "color_item.hpp"

namespace ArduinoGUI
{
class Elips
	: public ColorItem
{
public:
	void drawSelf(BaseDrawer& drawer) override;
	bool isTransparent() const override { return true; }
};
}

