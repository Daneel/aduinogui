#pragma once

#include "item.hpp"

namespace ArduinoGUI
{
class ColorItem
	: public Item
{
public:

	void setColor(Color value);
	Color getColor() const;

private:
	Color m_color = WHITE;
};
}


