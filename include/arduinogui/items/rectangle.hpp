#pragma once

#include "color_item.hpp"

namespace ArduinoGUI
{
class Rectangle
	: public ColorItem
{
public:
	void drawSelf(BaseDrawer& drawer) override;
	bool isTransparent() const override { return false; }
};
}


