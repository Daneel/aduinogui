#pragma once

#include "arduino_application.hpp"
#include "opensmart_tft_drawer.hpp"
#include "item_system.hpp"

namespace ArduinoGUI
{
class ArduinoGUIApplication
	: public ArduinoApplication
{
public:
	ArduinoGUIApplication();

	Rectangle& getRootItem() { return m_itemSystem.getRootItem(); }

private:
	OpensmartTFTDrawer m_drawSystem;
	ItemSystem m_itemSystem;
};
}
