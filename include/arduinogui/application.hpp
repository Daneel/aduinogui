#pragma once

#include "system.hpp"

#include <etl/intrusive_forward_list.h>

namespace ArduinoGUI
{
class Application
{
public:
	void addSystem(System& system);

	void setup();
	void tick(uint32_t dt);
private:
	etl::intrusive_forward_list<System> m_systems;
	etl::intrusive_forward_list<System>::iterator m_last = m_systems.end();
};
}
