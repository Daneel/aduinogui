#pragma once

#include "application.hpp"
#include "system.hpp"

#include <etl/intrusive_forward_list.h>

namespace ArduinoGUI
{
class ArduinoApplication
	: public Application
{
	using Base = Application;

public:
	void setup();
	void tick();

private:
	uint32_t m_time = 0;
};
}
