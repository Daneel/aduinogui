#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>
#include <TouchScreen.h>

MCUFRIEND_kbv tft;


// most mcufriend shields use these pins and Portrait mode:
uint8_t YP = A1;  // must be an analog pin, use "An" notation!
uint8_t XM = A2;  // must be an analog pin, use "An" notation!
uint8_t YM = 7;   // can be a digital pin
uint8_t XP = 6;   // can be a digital pin

uint16_t TS_LEFT = 900;
uint16_t TS_RT  = 150;
uint16_t TS_TOP = 950;
uint16_t TS_BOT = 100;

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 260 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 260);
TSPoint tp;

int x = 0;
int y = 0;

int tmp = 0;
#define SWAP(a, b) { tmp = a; a = b; b = tmp;}

uint8_t Orientation = 2;    //PORTRAIT

#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

class Circlue
{
public:

    void draw(MCUFRIEND_kbv& device)
    {
        if (!m_changed)
        {
            return;
        }

        device.drawCircle(m_lastDrawerX, m_lastDrawerY, 10, WHITE);
        device.drawCircle(m_x, m_y, 10, RED);
        m_lastDrawerX = m_x;
        m_lastDrawerY = m_y;
        m_changed = false;
    }

    void setX(int x)
    {
        m_x = x;
        m_changed = true;
    }

    void setY(int y)
    {
        m_y = y;
        m_changed = true;
    }

private:
    bool m_changed = true;
    int m_x = 50;
    int m_y = 50;
    int m_lastDrawerX = 0;
    int m_lastDrawerY = 0;
} circle;

void setup() {
    tft.begin(9600);
    tft.reset();
    tft.begin(0x65);
    tft.setRotation(Orientation);

    ts = TouchScreen(XP, YP, XM, YM, 260);
    switch (Orientation) {      // adjust for different aspects
        case 0:   break;        //no change,  calibrated for PORTRAIT
        case 1:   tmp = TS_LEFT, TS_LEFT = TS_BOT, TS_BOT = TS_RT, TS_RT = TS_TOP, TS_TOP = tmp;  break;
        case 2:   SWAP(TS_LEFT, TS_RT);  SWAP(TS_TOP, TS_BOT); break;
        case 3:   tmp = TS_LEFT, TS_LEFT = TS_TOP, TS_TOP = TS_RT, TS_RT = TS_BOT, TS_BOT = tmp;  break;
    }

    tft.fillScreen(0xFFFF);
    delay(1000);
}

// the loop function runs over and over again forever
void loop()
{
    tp = ts.getPoint();
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    pinMode(XP, OUTPUT);
    pinMode(YM, OUTPUT);

    if (tp.z > ts.pressureThreshhold)
    {
        const int16_t xpos = map(tp.x, TS_LEFT, TS_RT, tft.width(), 0);
        const int16_t ypos = map(tp.y, TS_TOP, TS_BOT, tft.height(), 0);

        circle.setX(xpos);
        circle.setY(ypos);
    }

    circle.draw(tft);
}

