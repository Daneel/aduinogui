#include <Arduino.h>

#include <etl/vector.h>
#include <etl/stl/utility.h>

etl::vector<std::pair<int, int>, 10> s_blinkScenario;

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);

    s_blinkScenario.push_back({100, 10});
    s_blinkScenario.push_back({1000, 3});
    s_blinkScenario.push_back({200, 5});
}

// the loop function runs over and over again forever
void loop()
{
    for (auto&[delayLength, blinkCount] : s_blinkScenario)
    {
        for (std::size_t i=0; i<blinkCount; i++)
        {
            digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
            delay(delayLength);                       // wait for a second
            digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
            delay(delayLength);                       // wait for a second
        }
    }
}
