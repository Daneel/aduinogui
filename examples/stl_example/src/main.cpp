#include <Arduino.h>

#include <vector>
#include <string>

std::vector<std::pair<int, int>> s_blinkScenario;

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);
    s_blinkScenario.push_back({20, 20});
    s_blinkScenario.push_back({40, 10});
    s_blinkScenario.push_back({80, 5});
    s_blinkScenario.push_back({160, 2});
}

// the loop function runs over and over again forever
void loop()
{
    for (auto&[delayLength, blinkCount] : s_blinkScenario)
    {
        for (std::size_t i=0; i<blinkCount; i++)
        {
            digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
            delay(delayLength);                       // wait for a second
            digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
            delay(delayLength);                       // wait for a second
        }
    }
}
