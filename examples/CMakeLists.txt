cmake_minimum_required(VERSION 3.8.2)

project(arduinogui_examples C CXX ASM)

add_subdirectory(blink)
add_subdirectory(stl_example)
add_subdirectory(etl_example)
add_subdirectory(open_smart_display_example)
add_subdirectory(touch_screen_example)
add_subdirectory(animation_example)
add_subdirectory(drawer_example)
add_subdirectory(arduino_gui_application_example)