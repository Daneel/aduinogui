cmake_minimum_required(VERSION 3.8.2)

include(TargetFindAndLinkOpenSmartTFT)

project(animation_example LANGUAGES C CXX ASM)

arduino_cmake_project(animation_example BOARD_NAME ${BOARD_NAME} BOARD_CPU ${BOARD_CPU})

add_arduino_executable(animation_example
    src/main.cpp
    src/draw_system.hpp
    src/draw_system.cpp
    src/my_application.hpp
    src/my_application.cpp
    )

target_link_libraries(animation_example PRIVATE arduinogui)

if(UPLOAD_PORT)
    set_target_upload_port(animation_example ${UPLOAD_PORT})
endif()

install(
    TARGETS animation_example
    EXPORT animation_example
    RUNTIME
        DESTINATION bin/examples
)