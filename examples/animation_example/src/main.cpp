#include <Arduino.h>

#include "my_application.hpp"

MyApplication application;

void setup()
{
	application.setup();
}

// the loop function runs over and over again forever
void loop()
{
	application.tick();
}

