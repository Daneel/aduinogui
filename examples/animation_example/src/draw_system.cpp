#include "draw_system.hpp"

#include <arduinogui/color.hpp>

namespace
{
constexpr uint16_t CIRCLE_RADIUS = 2;
constexpr uint8_t CIRCLE_COUNT = 8;
constexpr uint16_t ANIMATION_RADIUS = 100;
}

void DrawSystem::setup()
{
	System::setup();

	m_tft.begin(0x65);//to enable HX8352B driver code
	m_tft.setRotation(3);

	m_animationCenter.x = m_tft.width() / 2;
	m_animationCenter.y = m_tft.height() / 2;

	m_tft.fillScreen(ArduinoGUI::WHITE);
}
void DrawSystem::tick(uint32_t dTime)
{
	System::tick(dTime);

	const float previusOffset = m_time * 2 * PI / 10.0f;

	m_time += dTime / 1000.0f;
	const float angleOffset = m_time * 2 * PI / 10.0f;

	const float angleStep = 2 * PI / CIRCLE_COUNT;

	for (uint8_t i = 0; i < CIRCLE_COUNT; ++i)
	{
		const float previusAngle = previusOffset + i * angleStep;
		const float previusX = m_animationCenter.x + sin(previusAngle) * ANIMATION_RADIUS;
		const float previusY = m_animationCenter.y + cos(previusAngle) * ANIMATION_RADIUS;

		m_tft.fillCircle(previusX, previusY, CIRCLE_RADIUS, ArduinoGUI::WHITE);
	}

	for (uint8_t i = 0; i < CIRCLE_COUNT; ++i)
	{
		const float angle = angleOffset + i * angleStep;
		const float x = m_animationCenter.x + sin(angle) * ANIMATION_RADIUS;
		const float y = m_animationCenter.y + cos(angle) * ANIMATION_RADIUS;

		m_tft.fillCircle(x, y, CIRCLE_RADIUS, ArduinoGUI::BLUE);
	}

}

