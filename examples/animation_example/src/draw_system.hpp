#include <arduinogui/system.hpp>
#include <arduinogui/point.hpp>

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>

class DrawSystem
	: public ArduinoGUI::System
{
public:
	void setup() override;
	void tick(uint32_t dTime) override;
private:
	MCUFRIEND_kbv m_tft;
	float m_time = 0.0f;
	ArduinoGUI::Point m_animationCenter;
};
