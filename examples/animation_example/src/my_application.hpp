#pragma once

#include "draw_system.hpp"

#include <arduinogui/arduino_application.hpp>

class MyApplication
	: public ArduinoGUI::ArduinoApplication
{
public:
	MyApplication();

private:
	DrawSystem m_drawSystem;
};

