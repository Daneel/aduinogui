#include <Arduino.h>

#include <arduinogui/opensmart_tft_drawer.hpp>

ArduinoGUI::OpensmartTFTDrawer s_drawer;

void setup()
{
	Serial.begin(9600);
	s_drawer.setup();
}

// the loop function runs over and over again forever
void loop()
{
	const auto width = s_drawer.getContextWidth();
	const auto height = s_drawer.getContextHeight();

	s_drawer.beginDraw();

	s_drawer.drawRectangle(width, height, ArduinoGUI::BLACK);
	s_drawer.drawRectangle(100, 100, ArduinoGUI::RED);

	s_drawer.pushOffset({100, 100});
	s_drawer.drawRectangle(100, 100, ArduinoGUI::GREEN);
	s_drawer.popOffset();

	s_drawer.pushOffset({100, 0});
	s_drawer.drawElips(200, 100, ArduinoGUI::WHITE);
	s_drawer.popOffset();

	s_drawer.pushOffset({0, 100});
	s_drawer.drawText("Hellow World!", 1, ArduinoGUI::YELLOW);
	s_drawer.popOffset();

	s_drawer.pushOffset({0, 120});
	s_drawer.pushClipRect({100, 100});
	Serial.print("width:" + String(s_drawer.getClipRect().width) + "\n");
	Serial.print("height:" + String(s_drawer.getClipRect().height) + "\n");
	s_drawer.drawText("Hellow World!", 2, ArduinoGUI::YELLOW);
	s_drawer.popClipRect();
	s_drawer.popOffset();

	s_drawer.pushOffset({0, 150});
	s_drawer.pushClipRect({100, 100});
	s_drawer.drawText("Hellow World!", 3, ArduinoGUI::YELLOW);
	s_drawer.popClipRect();
	s_drawer.popOffset();

	s_drawer.endDraw();

	delay(3000);
}

