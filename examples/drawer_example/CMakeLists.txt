cmake_minimum_required(VERSION 3.8.2)

include(TargetFindAndLinkOpenSmartTFT)

project(drawer_example LANGUAGES C CXX ASM)

arduino_cmake_project(drawer_example BOARD_NAME ${BOARD_NAME} BOARD_CPU ${BOARD_CPU})

add_arduino_executable(drawer_example
    src/main.cpp
)

target_link_libraries(drawer_example PRIVATE arduinogui)

if(UPLOAD_PORT)
    set_target_upload_port(drawer_example ${UPLOAD_PORT})
endif()

install(
    TARGETS drawer_example
    EXPORT drawer_example
    RUNTIME
        DESTINATION bin/examples
)