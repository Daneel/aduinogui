cmake_minimum_required(VERSION 3.8.2)

project(arduino_gui_application_example LANGUAGES C CXX ASM)

arduino_cmake_project(arduino_gui_application_example BOARD_NAME ${BOARD_NAME} BOARD_CPU ${BOARD_CPU})

add_arduino_executable(arduino_gui_application_example
    src/main.cpp
    src/my_application.hpp
    src/my_application.cpp
    src/ui.hpp
    src/ui.cpp
)

target_link_libraries(arduino_gui_application_example PRIVATE arduinogui)

if(UPLOAD_PORT)
    set_target_upload_port(arduino_gui_application_example ${UPLOAD_PORT})
endif()

install(
    TARGETS arduino_gui_application_example
    EXPORT arduino_gui_application_example
    RUNTIME
        DESTINATION bin/examples
)