#include "ui.hpp"

#include <Arduino.h>

namespace
{
constexpr float	BIG_CIRCLE_TIME = 20.0f;
constexpr float	SMALL_CIRCLE_TIME = 10.0f;
constexpr uint32_t	BIG_CIRCLE_RADIUS = 30;
constexpr uint32_t	SMALL_CIRCLE_RADIUS = 10;
constexpr uint32_t	BIG_ANIMATION_RADIUS = 100;
constexpr uint32_t	SMALL_ANIMATION_RADIUS = 50;
constexpr uint32_t	SMALL_ANIMATION_RADIUS_EXCEED = 30;
}

UI::UI()
{
	setColor(ArduinoGUI::RED);
	m_bigChild.setColor(ArduinoGUI::BLUE);
	m_smallchild.setColor(ArduinoGUI::GREEN);

	setWidth(SMALL_ANIMATION_RADIUS * 2 + SMALL_CIRCLE_RADIUS * 2 - SMALL_ANIMATION_RADIUS_EXCEED);
	setHeight(SMALL_ANIMATION_RADIUS * 2 + SMALL_CIRCLE_RADIUS * 2 - SMALL_ANIMATION_RADIUS_EXCEED);

	m_bigChild.setWidth(BIG_CIRCLE_RADIUS);
	m_bigChild.setHeight(BIG_CIRCLE_RADIUS);

	m_smallchild.setWidth(SMALL_CIRCLE_RADIUS);
	m_smallchild.setHeight(SMALL_CIRCLE_RADIUS);

	m_bigChild.setX(getWidth() / 2 - BIG_CIRCLE_RADIUS / 2);
	m_bigChild.setY(getHeight() / 2 - BIG_CIRCLE_RADIUS / 2);

	addChild(m_bigChild);
	addChild(m_smallchild);
}

void UI::tickSelf(uint32_t dt)
{
	Base::tickSelf(dt);

	float dtF = dt / 1000.0f;

	ArduinoGUI::Point parentCenter;
	
	if (auto* parent = getParent())
	{
		parentCenter = ArduinoGUI::Point(parent->getWidth() / 2, parent->getHeight() / 2);
	}

	m_selfAngle += dtF * 2 * PI / BIG_CIRCLE_TIME;
	m_smallChildAngle += dtF * 2 * PI / SMALL_CIRCLE_TIME;

	uint32_t selfX = parentCenter.x + sin(m_selfAngle) * BIG_ANIMATION_RADIUS;
	uint32_t selfY = parentCenter.y + cos(m_selfAngle) * BIG_ANIMATION_RADIUS;

	uint32_t childX = getWidth() / 2 + sin(m_smallChildAngle) * SMALL_ANIMATION_RADIUS;
	uint32_t childY = getHeight() / 2 + cos(m_smallChildAngle) * SMALL_ANIMATION_RADIUS;

	m_smallchild.setX(childX);
	m_smallchild.setY(childY);

	setX(selfX - getWidth() / 2);
	setY(selfY - getHeight() / 2);
}


