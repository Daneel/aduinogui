#pragma once

#include <arduinogui/items/rectangle.hpp>
#include <arduinogui/items/elips.hpp>

class UI
	: public ArduinoGUI::Rectangle
{
	using Base = ArduinoGUI::Rectangle;

public:
	UI();

	void tickSelf(uint32_t dt) override;

private:
	ArduinoGUI::Elips m_bigChild;
	ArduinoGUI::Elips m_smallchild;
	float m_selfAngle = 0.0f;
	float m_smallChildAngle = 0.0f;
};
