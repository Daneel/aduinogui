#pragma once

#include "ui.hpp"

#include <arduinogui/arduino_gui_application.hpp>

class MyApplication
	: public ArduinoGUI::ArduinoGUIApplication
{
public:
	MyApplication();

private:
	UI m_ui;
};

