#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>

MCUFRIEND_kbv tft;

#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF


void setup() {
    tft.begin(0x65);//to enable HX8352B driver code
    tft.setRotation(3);
}

// the loop function runs over and over again forever
void loop() {
    tft.fillScreen(BLACK);
    delay(1000);
    tft.fillScreen(BLUE);
    delay(1000);
    tft.fillScreen(RED);
    delay(1000);
    tft.fillScreen(GREEN);
    delay(1000);
    tft.fillScreen(CYAN);
    delay(1000);
    tft.fillScreen(MAGENTA);
    delay(1000);
    tft.fillScreen(YELLOW);
    delay(1000);
    tft.fillScreen(WHITE);
    delay(1000);
}

